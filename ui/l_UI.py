# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_login_form(object):
    def setupUi(self, login_form):
        login_form.setObjectName("login_form")
        login_form.resize(242, 139)
        login_form.setMinimumSize(QtCore.QSize(242, 139))
        login_form.setMaximumSize(QtCore.QSize(242, 139))
        self.connection_group = QtWidgets.QGroupBox(login_form)
        self.connection_group.setGeometry(QtCore.QRect(0, 0, 241, 141))
        self.connection_group.setObjectName("connection_group")
        self.host_text = QtWidgets.QLineEdit(self.connection_group)
        self.host_text.setGeometry(QtCore.QRect(72, 20, 161, 22))
        self.host_text.setObjectName("host_text")
        self.username_text = QtWidgets.QLineEdit(self.connection_group)
        self.username_text.setGeometry(QtCore.QRect(72, 50, 161, 22))
        self.username_text.setObjectName("username_text")
        self.password_text = QtWidgets.QLineEdit(self.connection_group)
        self.password_text.setGeometry(QtCore.QRect(72, 80, 161, 22))
        self.password_text.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_text.setObjectName("password_text")
        self.label = QtWidgets.QLabel(self.connection_group)
        self.label.setGeometry(QtCore.QRect(10, 22, 41, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.connection_group)
        self.label_2.setGeometry(QtCore.QRect(10, 52, 41, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.connection_group)
        self.label_3.setGeometry(QtCore.QRect(10, 82, 55, 16))
        self.label_3.setObjectName("label_3")
        self.connect_btn = QtWidgets.QPushButton(self.connection_group)
        self.connect_btn.setGeometry(QtCore.QRect(140, 107, 93, 28))
        self.connect_btn.setObjectName("connect_btn")

        self.retranslateUi(login_form)
        QtCore.QMetaObject.connectSlotsByName(login_form)

    def retranslateUi(self, login_form):
        _translate = QtCore.QCoreApplication.translate
        login_form.setWindowTitle(_translate("login_form", "Login"))
        self.connection_group.setTitle(_translate("login_form", "Connection"))
        self.label.setText(_translate("login_form", "Host:"))
        self.label_2.setText(_translate("login_form", "Login:"))
        self.label_3.setText(_translate("login_form", "Password:"))
        self.connect_btn.setText(_translate("login_form", "Connect"))

