from PyQt5 import QtWidgets, QtGui, QtCore
from ui.UI import Ui_main_wind
import src.sql as c
import src._ops as o

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, sql, parent = None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_main_wind()
        self.ui.setupUi(self)
        self.sq = sql
        self.ops = o._ops(self.sq, self.ui, self)

        self.ops._set_statusbar("%s | %s | %s" % (self.sq.result, self.sq.static_data[0], self.sq.static_data[1]), g=255)

        self.ui.tree_header.setText(self.sq.static_data[1])
        self.ui.DBMS_tree.setColumnCount(1)
        self.ui.DBMS_tree.setHeaderLabels(["Databases"])

        self.ops._fill_tree(self.sq._get_db_tree())

        self.ui.DBMS_tree.itemDoubleClicked.connect(self.ops._load_table)
        self.ui.DBMS_tree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.DBMS_tree.customContextMenuRequested.connect(self.ops._DB_Tree_MENU)
        self.ui.db_table.customContextMenuRequested.connect(self.ops._DB_table_MENU)
        self.ui.db_table.itemChanged.connect(self.ops._change_table)
        self.ui.db_table.itemDoubleClicked.connect(self.ops._get_prev_val)