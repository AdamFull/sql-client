import mysql.connector

class sql(object):
    def __init__(self, host, username, password):
        super(sql,self)
        self.static_data = (host, username, password)
        self.connection_status, self.db_c, self.result = self._connect(host=host, username=username, password=password)
        try:
            self.cursor = self.db_c.cursor(buffered=True)
        except Exception as e:
            print(e)
    
    def _connect(self, host = '', username = '', password = ''):
        try:
            db = mysql.connector.connect(host=host ,user=username ,passwd=password)
            return True, db, 'Successful connection.'
        except Exception as e:
            return False, None, e

    def _get_db_tree(self):
        datas = [[],[]]
        self.cursor.execute("show databases")
        dbs = self.cursor.fetchall()

        for i in range(len(dbs)): # load database
            datas[0].append(dbs[i][0])
            self.cursor.execute("USE %s" % (dbs[i][0]))
            self.cursor.execute("SHOW TABLES")
            temp_array = []
            for (table_name) in self.cursor:
                temp_array.append(table_name[0])
            datas[1].append(temp_array)
            del temp_array
        del dbs
        return datas
    
    def _get_table(self, db, table):
        datas = []
        self.cursor.execute("USE %s" % (db))
        self.cursor.execute("SHOW columns FROM %s" % (table))
        columns = self.cursor.fetchall()
        temp_array = []
        for i in range(len(columns)):
            temp_array.append(columns[i][0])
        datas.append(temp_array)
        del temp_array

        self.cursor.execute("SELECT * FROM %s" % (table))
        result = self.cursor.fetchall()
        for table_t in result:
            temp_array = []
            for i in range(len(table_t)):
                temp_array.append(table_t[i])
            datas.append(temp_array)
            del temp_array
        return datas

    def _update(self, db, table, row, prev, new):
        self.cursor.execute("USE %s" % (db))
        self.cursor.execute("UPDATE {0} SET {3} = '{1}' WHERE {3} = '{2}'".format(table, prev, new, row))
    
    def _add_row(self, db, table, columns):
        self.cursor.execute("USE %s" % (db))
        self.cursor.execute("INSERT INTO %s (%s) VALUES ('')" % (table, columns[0]))
        self.db_c.commit()
    
    def _del_row(self, db, table, columns, value):
        self.cursor.execute("USE %s" % (db))
        self.cursor.execute("DELETE FROM %s WHERE %s = %s" % (table, columns[0], value))
        self.db_c.commit()
    
    def _new_db(self, db):
        self.cursor.execute("CREATE DATABASE %s" % (db))
        self.db_c.commit()
    
    def _del_db(self, db):
        self.cursor.execute("DROP DATABASE %s" % (db))
        self.db_c.commit()
    
    def _new_table(self):
        pass

    def _del_table(self, db, table):
        self.cursor.execute("USE %s" % (db))
        self.cursor.execute("DROP TABLE %s" % (table))
        self.db_c.commit()