from PyQt5 import QtWidgets, QtGui

class _ops(object):
    def __init__(self, sql, ui, self_obj):
        super(_ops, self).__init__()
        self.sq, self.ui, self.self_obj = sql, ui, self_obj
        self.d = {'prev_table_item':'', 'current_db':'', 'current_table':'', 'column_header':'', 'table_cols':[]}
        self.is_table_loaded = [False]
    
    def _set_statusbar(self, text, r = 0, g = 0, b = 0):
        self.ui.statusbar.setStyleSheet("QStatusBar{padding-left:8px;background:rgba(%s,%s,%s,255);color:black;font-weight:bold;}" % (r,g,b))
        self.ui.statusbar.showMessage(text)

    def _fill_table(self, table_list, net_bool):
        net_bool[0] = False
        self.ui.db_table.setColumnCount(len(table_list[0]))
        self.ui.db_table.setRowCount(len(table_list)-1)
        for i in range(0 ,len(table_list)):
            if i == 0:
                self.ui.db_table.setHorizontalHeaderLabels(table_list[i])
            else:
                for j in range(len(table_list[i])):
                    if not str(table_list[i][j]):
                        self.ui.db_table.setItem(i-1, j, QtWidgets.QTableWidgetItem(str(" ")))
                        self.ui.db_table.item(i-1, j).setBackground(QtGui.QColor(100,100,150))
                    else:
                        self.ui.db_table.setItem(i-1, j, QtWidgets.QTableWidgetItem(str(table_list[i][j])))
        self.ui.db_table.resizeColumnsToContents()
        net_bool[0] = True
    
    def _fill_tree(self, tree_list):
        self.ui.DBMS_tree.clear()
        for i in range(len(tree_list[0])):
            l = QtWidgets.QTreeWidgetItem([tree_list[0][i]])
            for j in range(len(tree_list[1][i])):
                l_child = QtWidgets.QTreeWidgetItem([tree_list[1][i][j]])
                l.addChild(l_child)
            self.ui.DBMS_tree.addTopLevelItem(l)
            del l

    def _input_db(self):
        text, ok = QtWidgets.QInputDialog.getText(self.self_obj, 'New database', 'Enter new db name:')
        if ok:
            try:
                self.sq._new_db(text)
                self._fill_tree(self.sq._get_db_tree())
                self.ui.db_table.clear()
            except Exception as e:
                self.error_print(e)
    
    def _del_db(self):
        res = QtWidgets.QMessageBox.question(self.self_obj, 'Are you sure?', 'Delete %s?' % (self.selected_db), QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if res == QtWidgets.QMessageBox.Yes:
            try:
                self.sq._del_db(self.selected_db)
                self._fill_tree(self.sq._get_db_tree())
                self.ui.db_table.clear()
            except Exception as e:
                self.error_print(e)
    
    def _del_table(self):
        res = QtWidgets.QMessageBox.question(self.self_obj, 'Are you sure?', 'Delete %s?' % (self.selected_table), QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        if res == QtWidgets.QMessageBox.Yes:
            try:
                self.sq._del_table(self.selected_db, self.selected_table)
                self._fill_tree(self.sq._get_db_tree())
                self.ui.db_table.clear()
            except Exception as e:
                self.error_print(e)

    def _DB_Tree_MENU(self, position):
        popup_menu = QtWidgets.QMenu()

        if len(self.ui.DBMS_tree.selectedItems()) == 0:
            popup_menu.addAction("New db", self._input_db)
        else:
            if self.ui.DBMS_tree.selectedItems()[0].child(0) != None:
                self.selected_db = self.ui.DBMS_tree.selectedItems()[0].text(0)
                popup_menu.addAction("New table", self.sq._new_table)
                popup_menu.addAction("Del db", self._del_db)
                self.ui.DBMS_tree.clearSelection()
            else:
                if self.ui.DBMS_tree.selectedItems()[0].child(0) == None and self.ui.DBMS_tree.selectedItems()[0].parent() != None:
                    self.selected_table = self.ui.DBMS_tree.selectedItems()[0].text(0)
                    self.selected_db = self.ui.DBMS_tree.selectedItems()[0].parent().text(0)
                    popup_menu.addAction("Del table", self._del_table)
                    self.ui.DBMS_tree.clearSelection()
                else:
                    self.selected_db = self.ui.DBMS_tree.selectedItems()[0].text(0)
                    popup_menu.addAction("New table", self.sq._new_table)
                    popup_menu.addAction("Del db", self._del_db)
                    self.ui.DBMS_tree.clearSelection()
        popup_menu.exec_(self.ui.DBMS_tree.viewport().mapToGlobal(position))
    
    def _DB_table_MENU(self, position):
        popup_menu = QtWidgets.QMenu()
        if len(self.ui.db_table.selectedItems()) == 0:
            popup_menu.addAction("Add row", self.sq._add_row(self.d['current_db'], self.d['current_table'], self.d['table_cols']))
        else:
            if self.ui.db_table.selectedItems()[0] != None:
                value = self.ui.db_table.selectedItems()[0].text()
                popup_menu.addAction("Delete row", self.sq._del_row(self.d['current_db'], self.d['current_table'], self.d['table_cols'], value))

        popup_menu.exec_(self.ui.db_table.viewport().mapToGlobal(position))
    
    def _del_row(self):
        # self.sq._del_row()
        pass

    def _add_row(self):
        pass


    def _get_prev_val(self, item):
        try:
            self.d['prev_table_item'] = item.text()
            self.d['column_header'] = self.ui.db_table.horizontalHeaderItem(item.column()).text()
        except Exception as e:
            self._set_statusbar(text=str(e), r=255)

    def _change_table(self, item):
        if self.is_table_loaded[0]:
            self.sq._update(self.d['current_db'], self.d['current_table'], self.d['column_header'], self.d['prev_table_item'], item.text())
            self.d['prev_table_item'] = ''
            self.d['column_header'] = ''

    def _load_table(self):
        getSelected = self.ui.DBMS_tree.currentItem()
        if getSelected:
            try:
                self.d['current_db'] = parent = getSelected.parent().text(0)
                self.d['current_table'] = child = getSelected.text(0)
                self.ui.db_table.clearSelection()
                self._fill_table(self.sq._get_table(db=parent, table=child), self.is_table_loaded)
                self.ui.table_header.setText("%s/%s" % (parent, child))
                for i in range(self.ui.db_table.columnCount()):
                    self.d['table_cols'].append(self.ui.db_table.horizontalHeaderItem(i).text())
                self._set_statusbar(text=str("Opened: %s/%s" % (parent, child)), g=255)
            except Exception as e:
                self.ui.table_header.setText("Error")
                self.ui.db_table.clear()
                self._set_statusbar(text=str(e), r=255)
    
    def error_print(self, err_msg):
        self.error_dialog = QtWidgets.QErrorMessage(self.self_obj)
        self.error_dialog.showMessage(str(err_msg))