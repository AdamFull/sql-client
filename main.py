from PyQt5 import QtWidgets, QtGui, QtCore
from ui.l_UI import Ui_login_form
from src._interface import MainWindow
import src.sql as c
import src._ops as o

# pyuic5 mainui.ui -o UI.py
# Изменить структуру временных данных

class Login(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Login, self).__init__(parent)
        self.ui = Ui_login_form()
        self.ui.setupUi(self)

        self.ui.connect_btn.clicked.connect(self.connect)

        self.ui.host_text.setText('localhost')
        self.ui.username_text.setText('root')
        self.ui.password_text.setText('')
    
    def connect(self):
        self.sql = c.sql(host=self.ui.host_text.text(), username=self.ui.username_text.text(), password=self.ui.password_text.text())
        self.ops = o._ops(self.sql, self, self)
        if self.sql.connection_status:
            self.accept()
        else:
            self.ops.error_print(self.sql.result)

def main():
    app = QtWidgets.QApplication([])
    login = Login()
    if login.exec_()==QtWidgets.QDialog.Accepted:
        application = MainWindow(sql=login.sql)
        application.show()
        app.exec()
	
if __name__ == "__main__":
    main()